'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var client = require('./client');
var Flow = require('./Flow');

var Intbot = function () {
  function Intbot() {
    _classCallCheck(this, Intbot);

    this.flows = {};
  }

  _createClass(Intbot, [{
    key: 'setRemote',
    value: function setRemote(remote) {
      client.setRemote(remote);
      return this;
    }
  }, {
    key: 'getRemote',
    value: function getRemote() {
      return client.getRemote();
    }
  }, {
    key: 'noStrictSSL',
    value: function noStrictSSL() {
      client.setStrictSSL(false);
      return this;
    }
  }, {
    key: 'strictSSL',
    value: function strictSSL() {
      client.setStrictSSL(true);
      return this;
    }
  }, {
    key: 'load',
    value: function load() {
      var _this = this;

      for (var _len = arguments.length, flowTokens = Array(_len), _key = 0; _key < _len; _key++) {
        flowTokens[_key] = arguments[_key];
      }

      return Promise.all(flowTokens.map(function (token) {
        return Promise.all([client.getFlowProcess(token), client.getFields(token)]).then(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
              _ref2$ = _ref2[0],
              statuses = _ref2$.statuses,
              steps = _ref2$.steps,
              fields = _ref2[1];

          _this.flows[token] = new Flow(token, statuses, steps, fields);
          return _this;
        });
      })).then(function () {
        return _this;
      });
    }
  }, {
    key: 'unload',
    value: function unload() {
      var _this2 = this;

      for (var _len2 = arguments.length, flowTokens = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        flowTokens[_key2] = arguments[_key2];
      }

      flowTokens.forEach(function (token) {
        delete _this2.flows[token];
      });

      return Promise.resolve(this);
    }
  }, {
    key: 'flow',
    value: function flow(token) {
      if (!(token in this.flows)) {
        throw new Error('invalid token \'' + token + '\'');
      }

      return this.flows[token];
    }
  }]);

  return Intbot;
}();

module.exports = new Intbot();