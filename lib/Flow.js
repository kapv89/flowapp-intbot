'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var client = require('./client');

var Flow = function () {
  function Flow(token, statuses, steps, fields) {
    _classCallCheck(this, Flow);

    this.token = token;
    this.statuses = statuses;
    this.steps = steps;
    this.fields = fields;
  }

  _createClass(Flow, [{
    key: 'field',
    value: function field(title) {
      return this.fields.find(function (field) {
        return field.title === title;
      });
    }
  }, {
    key: 'status',
    value: function status(name) {
      return this.statuses.find(function (status) {
        return status.name === name;
      });
    }
  }, {
    key: 'step',
    value: function step(fromStatusName, toStatusName) {
      return this.steps.find(function (step) {
        return step.from_status.name === fromStatusName && step.to_status.name === toStatusName;
      });
    }
  }, {
    key: 'getUsers',
    value: function getUsers() {
      return client.getUsers(this.token);
    }
  }, {
    key: 'getRecords',
    value: function getRecords() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      return client.getRecords(this.token, _extends({}, params, { view_permission: true }));
    }
  }, {
    key: 'getCount',
    value: function getCount() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      return client.getCount(this.token, _extends({}, params, { view_permission: true }));
    }
  }, {
    key: 'uploadRecords',
    value: function uploadRecords(step) {
      var records = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      return client.uploadRecords(this.token, step, records);
    }
  }, {
    key: 'updateRecord',
    value: function updateRecord(record) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      return client.updateRecord(this.token, record, data);
    }
  }, {
    key: 'deleteRecords',
    value: function deleteRecords() {
      var records = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      return client.deleteRecords(this.token, records);
    }
  }, {
    key: 'moveRecords',
    value: function moveRecords(step) {
      var records = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      return client.moveRecords(this.token, step, records);
    }
  }, {
    key: 'delegateRecords',
    value: function delegateRecords(user) {
      var records = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      return client.delegateRecords(this.token, user, records);
    }
  }]);

  return Flow;
}();

module.exports = Flow;