'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

// const request = require('request-promise');
var request = require('./request');

var remote = 'http://api.formapp.in/integration';
var strictSSL = false;

function makeRequest(slug, token) {
  var json = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  return request(_extends({
    method: 'POST',
    uri: '' + remote + slug,
    json: json,
    headers: { 'integration-token': token }
  }, remote.indexOf('https') === 0 ? { strictSSL: strictSSL } : {}));
}

function getRemote() {
  return remote;
}

function setRemote(newRemote) {
  remote = newRemote;
}

function getStrictSSL() {
  return strictSSL;
}

function setStrictSSL(flag) {
  strictSSL = flag;
}

function getFlowProcess(token) {
  return makeRequest('/process', token);
}

function getFields(token) {
  return makeRequest('/fields', token).then(function (_ref) {
    var fields = _ref.fields;
    return fields;
  });
}

function getUsers(token) {
  return makeRequest('/users', token).then(function (_ref2) {
    var users = _ref2.users;
    return users;
  });
}

function getRecords(token) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  return makeRequest('/records/list', token, params).then(function (_ref3) {
    var records = _ref3.records;
    return records;
  });
}

function getCount(token) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  return makeRequest('/records/count', token, params).then(function (_ref4) {
    var count = _ref4.count;
    return count;
  });
}

function uploadRecords(token, step) {
  var records = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  return makeRequest('/records/upload/' + step.id, token, records);
}

function updateRecord(token, record) {
  var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  return makeRequest('/records/update/' + record.id, token, data);
}

function deleteRecords(token) {
  var records = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  return makeRequest('/records/delete', token, { record_ids: records.map(function (_ref5) {
      var id = _ref5.id;
      return id;
    }) });
}

function moveRecords(token, step) {
  var records = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  return makeRequest('/records/move/' + step.id, token, { record_ids: records.map(function (_ref6) {
      var id = _ref6.id;
      return id;
    }) });
}

function delegateRecords(token, user) {
  var records = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  return makeRequest('/records/delegate-to/' + user.id, token, { record_ids: records.map(function (_ref7) {
      var id = _ref7.id;
      return id;
    }) });
}

module.exports = {
  getRemote: getRemote,
  setRemote: setRemote,
  getStrictSSL: getStrictSSL,
  setStrictSSL: setStrictSSL,
  getFlowProcess: getFlowProcess,
  getFields: getFields,
  getUsers: getUsers,
  getRecords: getRecords,
  getCount: getCount,
  uploadRecords: uploadRecords,
  updateRecord: updateRecord,
  deleteRecords: deleteRecords,
  moveRecords: moveRecords,
  delegateRecords: delegateRecords
};