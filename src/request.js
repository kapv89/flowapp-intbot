const sa = require('superagent');
const {merge} = require('lodash');

function request(options) {
  const req = sa[options.method.toLowerCase()](options.uri)
    .set(merge({}, options.headers, options.json ? {'Content-Type': 'application/json'} : {}))
    .query(options.qs ? options.qs : {})
    .send(options.json || options.form || options.formData)
  ;

  request.activeRequests.add(req);

  const reqPromise = new Promise((resolve, reject) => {
    req.end((err, res) => {
      request.activeRequests.delete(req);

      if (err || !res.ok) {
        const error = res ? res : err;
        console.log(error, options.uri);
        reject(error);
      } else {
        resolve(res.body);
      }
    });
  });

  reqPromise.abort = (...args) => {
    request.activeRequests.delete(req);
    return req.abort(...args);
  };

  return reqPromise;
}

request.activeRequests = new Set();

module.exports = request;
