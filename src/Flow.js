const client = require('./client');

class Flow {
  constructor(token, statuses, steps, fields) {
    this.token = token;
    this.statuses = statuses;
    this.steps = steps;
    this.fields = fields;
  }

  field(title) {
    return this.fields.find((field) => field.title === title);
  }

  status(name) {
    return this.statuses.find((status) => status.name === name);
  }

  step(fromStatusName, toStatusName) {
    return this.steps.find((step) => step.from_status.name === fromStatusName && step.to_status.name === toStatusName);
  }

  getUsers() {
    return client.getUsers(this.token);
  }

  getRecords(params={}) {
    return client.getRecords(this.token, {...params, view_permission: true});
  }

  getCount(params={}) {
    return client.getCount(this.token, {...params, view_permission: true});
  }

  uploadRecords(step, records=[]) {
    return client.uploadRecords(this.token, step, records);
  }

  updateRecord(record, data={}) {
    return client.updateRecord(this.token, record, data);
  }

  deleteRecords(records=[]) {
    return client.deleteRecords(this.token, records);
  }

  moveRecords(step, records=[]) {
    return client.moveRecords(this.token, step, records);
  }

  delegateRecords(user, records=[]) {
    return client.delegateRecords(this.token, user, records);
  }
}

module.exports = Flow;
