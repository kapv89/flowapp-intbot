// const request = require('request-promise');
const request = require('./request');

let remote = 'http://api.formapp.in/integration';
let strictSSL = false;

function makeRequest(slug, token, json={}) {
  return request({
    method: 'POST',
    uri: `${remote}${slug}`,
    json,
    headers: {'integration-token': token},
    ...(
      remote.indexOf('https') === 0 ?
        {strictSSL} :
        {}
    )
  });
}

function getRemote() {
  return remote;
}

function setRemote(newRemote) {
  remote = newRemote;
}

function getStrictSSL() {
  return strictSSL;
}

function setStrictSSL(flag) {
  strictSSL = flag;
}

function getFlowProcess(token) {
  return makeRequest('/process', token);
}

function getFields(token) {
  return makeRequest('/fields', token).then(({fields}) => fields);
}

function getUsers(token) {
  return makeRequest('/users', token).then(({users}) => users);
}

function getRecords(token, params={}) {
  return makeRequest('/records/list', token, params).then(({records}) => records);
}

function getCount(token, params={}) {
  return makeRequest('/records/count', token, params).then(({count}) => count);
}

function uploadRecords(token, step, records=[]) {
  return makeRequest(`/records/upload/${step.id}`, token, records);
}

function updateRecord(token, record, data={}) {
  return makeRequest(`/records/update/${record.id}`, token, data);
}

function deleteRecords(token, records=[]) {
  return makeRequest('/records/delete', token, {record_ids: records.map(({id}) => id)});
}

function moveRecords(token, step, records=[]) {
  return makeRequest(`/records/move/${step.id}`, token, {record_ids: records.map(({id}) => id)});
}

function delegateRecords(token, user, records=[]) {
  return makeRequest(`/records/delegate-to/${user.id}`, token, {record_ids: records.map(({id}) => id)});
}

module.exports = {
  getRemote,
  setRemote,
  getStrictSSL,
  setStrictSSL,
  getFlowProcess,
  getFields,
  getUsers,
  getRecords,
  getCount,
  uploadRecords,
  updateRecord,
  deleteRecords,
  moveRecords,
  delegateRecords
};
