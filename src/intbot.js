const client = require('./client');
const Flow = require('./Flow');

class Intbot {
  constructor() {
    this.flows = {};
  }

  setRemote(remote) {
    client.setRemote(remote);
    return this;
  }

  getRemote() {
    return client.getRemote();
  }

  noStrictSSL() {
    client.setStrictSSL(false);
    return this;
  }

  strictSSL() {
    client.setStrictSSL(true);
    return this;
  }

  load(...flowTokens) {
    return Promise.all(flowTokens.map((token) => {
      return Promise.all([client.getFlowProcess(token), client.getFields(token)])
        .then(([{statuses, steps}, fields]) => {
          this.flows[token] = new Flow(token, statuses, steps, fields);
          return this;
        });
    })).then(() => this);
  }

  unload(...flowTokens) {
    flowTokens.forEach((token) => {
      delete this.flows[token];
    });

    return Promise.resolve(this);
  }

  flow(token) {
    if (!(token in this.flows)) {
      throw new Error(`invalid token '${token}'`);
    }

    return this.flows[token];
  }
}

module.exports = new Intbot();
